#[derive(Debug, Clone)]
struct PairsState<Item, I2> {
    first: Item,
    running_second: I2,
}

#[derive(Debug, Clone)]
pub struct Pairs<Item, I1, I2> {
    first: I1,
    original_second: I2,
    state: Option<PairsState<Item, I2>>,
}

impl<Item: Clone, I1: Iterator<Item = Item>, I2: Iterator<Item = Item> + Clone>
    Pairs<Item, I1, I2>
{
    fn advance_first(&mut self) -> Option<(Item, Item)> {
        match self.first.next() {
            None => None,
            Some(first) => {
                self.state = Some(PairsState {
                    first,
                    running_second: self.original_second.clone(),
                });
                self.next()
            }
        }
    }
}

impl<Item: Clone, I1: Iterator<Item = Item>, I2: Iterator<Item = Item> + Clone> Iterator
    for Pairs<Item, I1, I2>
{
    type Item = (Item, Item);

    fn next(&mut self) -> Option<Self::Item> {
        match self.state {
            None => self.advance_first(),
            Some(ref mut state) => match state.running_second.next() {
                None => self.advance_first(),
                Some(second) => Some((state.first.clone(), second)),
            },
        }
    }
}

pub trait Cross<I2: Iterator<Item = Self::Item> + Clone>: Iterator + Sized {
    fn cross(self, i2: I2) -> Pairs<Self::Item, Self, I2> {
        Pairs {
            first: self,
            original_second: i2,
            state: None,
        }
    }
}

impl<Item: Clone, I1: Iterator<Item = Item>, I2: Iterator<Item = Item> + Clone> Cross<I2> for I1 {}
