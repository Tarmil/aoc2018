use crate::cross::Cross;
use aoc_runner_derive::*;
use derive_error::*;
use std::num::ParseIntError;

struct Claim {
    id: usize,
    left: usize,
    top: usize,
    width: usize,
    height: usize,
}

#[derive(Debug, Error)]
enum ParseClaimError {
    /// Failed to parse a field
    InvalidField(ParseIntError),
    /// Missing a field
    #[error(no_from, non_std)]
    MissingField(&'static str),
}

fn gen_claim(line: &str) -> Result<Claim, ParseClaimError> {
    use self::ParseClaimError::*;

    fn parse_field(s: Option<&str>, name: &'static str) -> Result<usize, ParseClaimError> {
        Ok(s.ok_or_else(|| MissingField(name))?.trim().parse()?)
    }

    // Format: "#123 @ 3,2: 5x4"
    let separators: &[char] = &['#', '@', ',', ':', 'x'];
    let mut items = line.split(separators);
    items.next().unwrap(); // Ignore before #
    Ok(Claim {
        id: parse_field(items.next(), "id")?,
        left: parse_field(items.next(), "left")?,
        top: parse_field(items.next(), "top")?,
        width: parse_field(items.next(), "width")?,
        height: parse_field(items.next(), "height")?,
    })
}

fn gen<'a>(input: &'a str) -> impl Iterator<Item = Claim> + 'a {
    input
        .lines()
        .map(gen_claim)
        .collect::<Result<Vec<Claim>, ParseClaimError>>()
        .unwrap()
        .into_iter()
}

const SIZE: usize = 1000;
const AREA: usize = SIZE * SIZE;

#[aoc(day3, part1)]
pub fn solve1(input: &str) -> i32 {
    let mut cloth = [0u8; AREA];
    for claim in gen(input) {
        for x in claim.left..claim.left + claim.width {
            for y in claim.top..claim.top + claim.height {
                cloth[SIZE * x + y] = cloth[SIZE * x + y].saturating_add(1);
            }
        }
    }
    cloth.iter().map(|&x| if x > 1 { 1 } else { 0 }).sum()
}

#[aoc(day3, part2)]
pub fn solve2(input: &str) -> usize {
    let mut cloth = [[0u8; SIZE]; SIZE];
    gen(input)
        .filter(|claim| {
            (claim.left..claim.left + claim.width)
                .cross(claim.top..claim.top + claim.height)
                .fold(true, |is_candidate, (x, y)| {
                    let current = cloth[x][y];
                    cloth[x][y] = current.saturating_add(1);
                    is_candidate && current == 0
                })
        })
        .collect::<Vec<Claim>>()
        .iter()
        .find(|claim| {
            (claim.left..claim.left + claim.width)
                .cross(claim.top..claim.top + claim.height)
                .all(|(x, y)| cloth[x][y] == 1)
        })
        .unwrap()
        .id
}
