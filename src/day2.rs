use std::collections::hash_map::*;
use std::collections::hash_map::Entry::*;
use super::pairs::AllPairs;
use aoc_runner_derive::*;

pub fn gen<'a>(input: &'a str) -> impl Iterator<Item = &'a str> + Clone + 'a {
    input.lines()
}

#[aoc(day2, part1)]
pub fn solve1(input: &str) -> i32 {
    let mut sum2 = 0;
    let mut sum3 = 0;
    for s in gen(input) {
        let mut seen = HashMap::new();
        let mut count_2 = 0;
        let mut count_3 = 0;
        for x in s.chars() {
            match seen.entry(x) {
                Occupied(mut e) => {
                    let count_x = e.get_mut();
                    match count_x {
                        1 => count_2 += 1,
                        2 => {
                            count_2 -= 1;
                            count_3 += 1;
                        }
                        3 => count_3 -= 1,
                        _ => (),
                    }
                    *count_x += 1;
                }
                Vacant(e) => {
                    e.insert(1);
                }
            }
        }
        if count_2 > 0 {
            sum2 += 1;
        }
        if count_3 > 0 {
            sum3 += 1;
        }
    }
    sum2 * sum3
}

#[aoc(day2, part2)]
pub fn solve2(input: &str) -> String {
    for (s, o) in gen(input).all_pairs() {
        let mut char_diff = s.char_indices().zip(o.chars()).filter(
            |((_, cs), co)| cs != co,
        );
        match (char_diff.next(), char_diff.next()) {
            (Some(((i, c), _)), None) => {
                let csize = c.len_utf8();
                let mut res = String::with_capacity(s.len() - csize);
                res.push_str(&s[..i]);
                res.push_str(&s[i + csize..]);
                return res;
            }
            _ => continue,
        }
    }
    unreachable!()
}
