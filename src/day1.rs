use std::collections::hash_set::*;
use aoc_runner_derive::*;

pub fn gen<'a>(input: &'a str) -> impl Iterator<Item = i32> + Clone + 'a {
    input.lines().flat_map(str::parse)
}

#[aoc(day1, part1)]
pub fn solve1(input: &str) -> i32 {
    gen(input).sum()
}


#[aoc(day1, part2)]
pub fn solve2(input: &str) -> i32 {
    let mut seen = HashSet::new();
    let mut sum = 0;
    for x in gen(input).cycle() {
        sum += x;
        if !seen.insert(sum) {
            return sum;
        }
    }
    unreachable!()
}
