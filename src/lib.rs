use aoc_runner_derive::*;

mod pairs;
mod cross;
pub mod day1;
pub mod day2;
pub mod day3;

aoc_lib!{ year = 2018 }
