#[derive(Debug, Clone)]
struct PairsState<Item, I> {
    second: Item,
    pos: usize,
    running_first: std::iter::Take<I>,
    running_second: I,
}

#[derive(Debug, Clone)]
pub struct Pairs<Item, I> {
    original: I,
    state: Option<PairsState<Item, I>>,
}

impl<Item: Clone, I: Iterator<Item = Item> + Clone> Iterator for Pairs<Item, I> {
    type Item = (Item, Item);

    fn next(&mut self) -> Option<Self::Item> {
        match self.state {
            None => {
                let mut running_second = self.original.clone();
                match running_second.next() {
                    None => None,
                    Some(first) => {
                        self.state = Some(PairsState {
                            second: first,
                            pos: 0,
                            running_first: self.original.clone().take(0),
                            running_second,
                        });
                        self.next()
                    }
                }
            }
            Some(ref mut state) => {
                match state.running_first.next() {
                    None => {
                        match state.running_second.next() {
                            None => {
                                self.state = None;
                                None
                            }
                            Some(second) => {
                                state.pos += 1;
                                state.running_first = self.original.clone().take(state.pos);
                                state.second = second.clone();
                                let first = state.running_first.next().unwrap();
                                Some((first, second))
                            }
                        }
                    }
                    Some(first) => Some((first, state.second.clone())),
                }
            }
        }
    }
}

pub trait AllPairs: Iterator + Sized {
    fn all_pairs(self) -> Pairs<Self::Item, Self> {
        Pairs {
            original: self,
            state: None,
        }
    }
}

impl<Item: Clone, I: Iterator<Item = Item> + Clone> AllPairs for I {}
